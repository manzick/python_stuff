import time
import pickle
import json


#Создание массива
arr = [0]
for i in range(1, 1000000):
    arr.append(i)


millis1 = int(round(time.time() * 1000))
with open('test.json', 'w') as json_file:
    json.dump(arr, json_file, ensure_ascii=False)
millis2 = int(round(time.time() * 1000))
millis = millis2 - millis1
print('Создание json: ' + str(millis))


millis1 = int(round(time.time() * 1000))
with open('test.dat', "wb") as file:
    pickle.dump(arr, file)
millis2 = int(round(time.time() * 1000))
millis = millis2 - millis1
print('Создание бинарного файла: ' + str(millis))


millis1 = int(round(time.time() * 1000))
with open('test.json') as json_file:
        test = json.load(json_file)
        for t in test:
            if t == 999999:
                millis2 = int(round(time.time() * 1000))
millis = millis2 - millis1
print('Получение последнего элемента json: ' + str(millis))


millis1 = int(round(time.time() * 1000))
with open('test.dat', "rb") as file:
    test = pickle.load(file)
    for t in test:
            if t == 999999:
                millis2 = int(round(time.time() * 1000))
millis = millis2 - millis1
print('Получение последнего элемента бинарного файла: ' + str(millis))